/**
 * by Jorge Adriano Branco Aires
 * based on instructional videos by Daniel Ciocîrlan @ Rock the JVM
 * with improved inductive definition of natural addition
 */

import naturals.add
import naturals.Nat._

import scala.reflect.runtime.universe._
object Main {

  def show[T](value: T)(implicit tag: TypeTag[T]): String =
    tag.toString().replace("naturals.Nat.", "")

  val zeroPlusTwoChecked: add.Plus[_0, _2, _2]  = add[_0, _2]
  val onePlusTwoChecked: add.Plus[_1, _2, _3]  = add[_1, _2]
  val threePlusTwoChecked: add.Plus[_3, _2, _5] = add[_3, _2]

  val zeroPlusTwo  = add[_0, _2]
  val onePlusTwo = add[_1, _2]
  val threePlusTwo = add[_3, _2]


  def main(args: Array[String]): Unit = {
    println(show(zeroPlusTwoChecked))
    println(show(onePlusTwoChecked))
    println(show(threePlusTwoChecked))
    println(show(zeroPlusTwo))
    println(show(onePlusTwo))
    println(show(threePlusTwo))
  }
}
