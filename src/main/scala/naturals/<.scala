package naturals

trait <[A <: Nat, B <: Nat]
object <{
  import Nat._

  implicit def ltBase[B <: Nat]: <[_0, Succ[B]] = new <[_0, Succ[B]] {}
  implicit def ltInd[A <:Nat, B <: Nat](implicit lt: <[A, B]): <[Succ[A], Succ[B]]  = new <[Succ[A], Succ[B]] {}
}
