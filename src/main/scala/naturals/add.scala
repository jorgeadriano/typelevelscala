package naturals

trait add[A <: Nat, B <: Nat] { type Result <: Nat }

object add {
  import Nat._

  type Plus[A <: Nat, B <: Nat, S <: Nat] = add[A, B] { type Result = S }

  implicit def plusZero[B <: Nat]: Plus[_0, B, B] =
    new add[_0, B] { type Result = B }
  implicit def plusSucc[A <: Nat, B <: Nat, S <: Nat](implicit plus: Plus[A, B, S]): Plus[Succ[A], B, Succ[S]] =
    new add[Succ[A], B] { type Result = Succ[S] }

  def apply[A <: Nat, B <: Nat](implicit plus: add[A, B]): Plus[A, B, plus.Result] = plus
}

// original version https://www.youtube.com/watch?v=EGAJJpGODHg

//object add {
//  import Nat._
//
//  type Plus[A <: Nat, B <: Nat, S <: Nat] = add[A, B] { type Result = S }
//  def apply[A <: Nat, B <: Nat](implicit plus: add[A, B]): Plus[A, B, plus.Result] = plus
//
//  implicit def zero: Plus[_0, _0, _0] =
//    new add[_0, _0] { type Result = _0 }
//
//  implicit def zeroLeft[A <: Nat](implicit lt: <[_0, A]): Plus[_0, A, A] =
//    new add[_0, A] { type Result = A }
//
//  implicit def zeroRight[A <: Nat](implicit lt: <[_0, A]): Plus[A, _0, A] =
//    new add[A, _0] { type Result = A }
//
//  implicit def plusSucc[A <: Nat, B <: Nat, S <: Nat](implicit plus: Plus[A, B, S]): Plus[Succ[A], Succ[B], Succ[Succ[S]]] =
//    new add[Succ[A], Succ[B]] { type Result = Succ[Succ[S]] }
//
//  //val z: Plus[_3, _2, _5] = apply
//
//}
